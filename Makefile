###########################################################
#                 COPYRIGHT                               #
#                 ---------                               #
#                                                         #
# See Copyright Notice in COPYING and license in LICENSE. #
###########################################################

# Edit the following 3 variables. RHOST is the fully qualified name of the SyncBox server. RUSER is the username of the owner of the SyncBox on RHOST.
# RREPOSITORY is the path of the SyncBox, relative to RUSER's home directory on RHOST.

RHOST		= host.domain.xx
RUSER		= joe
RREPOSITORY	= syncbox

# You should not change anything below this line... unless you perfectly know
# what you are doing, of course.

RSYNC		= rsync
HERE		= $(notdir $(CURDIR))
NOW		= $(shell date '+%s')
USER		= $(shell whoami)
HOST		= $(shell hostname)
TAG		= $(USER)@$(HOST)
BACKUPPREFIX	= .backup
BACKUPDIR	= $(BACKUPPREFIX).$(NOW)
LOCKFILE	= .lock
ifneq ($(strip $(DRYRUN)),)
DRYRUN		= --dry-run
endif
RSYNCFLAGS	= --verbose --recursive --checksum --backup --backup-dir=$(BACKUPDIR) --links --perms --times --delete --exclude="$(BACKUPPREFIX).*/" \
                  --exclude="$(LOCKFILE) $(DRYRUN)"

define SYNCBOXMAKEHELPSTRING
make help:  print this help. This is the default make target.

make init:  create the BOX on the server by copying the current directory.
            An error is raised if the BOX already exists on the server.
            Else synchronize with:
              $(RSYNC) $(RSYNCFLAGS) ./ $(RUSER)@$(RHOST):$(RREPOSITORY)

make open:  update the local BOX by making it identical to the remote BOX.
            First check local or remote '.lock' files. Abort if one is
            found (a local '.lock' file indicates that the BOX is already
            opened; a remote one indicates that another client already
            opened the BOX and is probably modifying it). Else, create the
            remote and local '.lock' files and synchronize with:
              $(RSYNC) $(RSYNCFLAGS) $(RUSER)@$(RHOST):$(RREPOSITORY) .

make close: update the remote BOX by making it identical to the local BOX.
            First check local or remote '.lock' files. Abort if one is
            not found (a missing '.lock' file indicates that the BOX has
            not been opened by the client. Else, delete the remote and
            local '.lock' files and synchronize with:
              $(RSYNC) $(RSYNCFLAGS) ./ $(RUSER)@$(RHOST):$(RREPOSITORY)

make sync:  synchronize an empty fresh local BOX with the remote one.
            Equivalent to 'make open close'."
endef

export SYNCBOXMAKEHELPSTRING

help:
	@echo "$$SYNCBOXMAKEHELPSTRING"

init:
	cmd="[ -d $(RREPOSITORY) ] && { echo '***** The remote repository already exists.'; exit 1; }; mkdir $(RREPOSITORY)"; \
	ssh $(RUSER)@$(RHOST) "$$cmd" || { echo "***** Aborting..."; exit 1; }; \
	$(RSYNC) $(RSYNCFLAGS) ./ $(RUSER)@$(RHOST):$(RREPOSITORY)

open:
	@[ -f $(LOCKFILE) ] && { echo "***** Found a lock file in the local copy:"; cat $(LOCKFILE); echo "***** Aborting..."; exit 1; }; \
	cmd="[ -f $(RREPOSITORY)/$(LOCKFILE) ] && { echo '***** Found a lock file in the remote repository:'; cat $(RREPOSITORY)/$(LOCKFILE); exit 1; }; echo $(TAG) 2> /dev/null > $(RREPOSITORY)/$(LOCKFILE) || { echo '***** Could not create lock file in the remote repository.'; exit 1; }"; \
	ssh $(RUSER)@$(RHOST) "$$cmd" || { echo "***** Aborting..."; exit 1; }; \
	echo $(TAG) 2> /dev/null > $(LOCKFILE) || { echo "***** Could not create local lock file."; echo "***** Aborting..."; exit 1; }; \
	$(RSYNC) $(RSYNCFLAGS) $(RUSER)@$(RHOST):$(RREPOSITORY)/ .

close:
	@[ -f $(LOCKFILE) ] || { echo "***** No lock file in the local copy."; echo "***** Aborting..."; exit 1; }; \
	cmd="[ -f $(RREPOSITORY)/$(LOCKFILE) ] || { echo '***** No lock file in the remote repository.'; exit 1; }"; \
	ssh $(RUSER)@$(RHOST) "$$cmd" || { echo "***** Aborting..."; exit 1; }; \
	$(RSYNC) $(RSYNCFLAGS) ./ $(RUSER)@$(RHOST):$(RREPOSITORY); \
	cmd="rm $(RREPOSITORY)/$(LOCKFILE) || { echo '***** Could not delete lock file in the remote repository.'; exit 1; }"; \
	ssh $(RUSER)@$(RHOST) "$$cmd" || { echo "***** Aborting..."; exit 1; }; \
	rm $(LOCKFILE) || { echo "***** Could not delete local lock file."; echo "***** Aborting..."; exit 1; }

sync:
	@[ -f $(LOCKFILE) ] && { echo "***** Found a lock file in the local copy:"; cat $(LOCKFILE); echo "***** Aborting..."; exit 1; }; \
	cmd="[ ! -f $(RREPOSITORY)/$(LOCKFILE) ] || { echo '***** Found a lock file in the remote repository:'; cat $(RREPOSITORY)/$(LOCKFILE); exit 1; };"; \
	ssh $(RUSER)@$(RHOST) "$$cmd" || { echo "***** Aborting..."; exit 1; }; \
	$(RSYNC) $(RSYNCFLAGS) $(RUSER)@$(RHOST):$(RREPOSITORY)/ .
