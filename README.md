# Table of content
1. [License](#license)
1. [Description](#Description)
1. [Manual](#Manual)
  * [Creating a new BOX](#New)
  * [Getting a fresh copy of an existing BOX on a new client](#Fresh)
  * [Using a BOX on a client](#Using)
  * [Deleting a copy of the BOX on a client](#Deleting)
  * [Completely deleting a BOX](#Nuke)
  * [Dry runs](#Dry)
  * [Error messages](#Errors)

# <a name="License"></a>License

Copyright Renaud Pacalet (renaud.pacalet@free.fr)

Licensed uder the CeCILL license, Version 2.1 of 2013-06-21 (the "License"). You should have received a copy of the License. Else, you may obtain a copy of the License at:

http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt

# <a name="Description"></a>Description

SyncBox is a very simple tool allowing to share a directory (the BOX) among any number of clients. The BOX has one reference copy on a server and as many working copies as needed on a number of clients. SyncBox manages the synchronization between the reference copy on the server and the working copies on the clients. It is a very convenient way to access information (pictures, musics, documents,...) from anywhere, and, when needed, to propagate modifications to the reference copy and to the other working copies. Several BOXes can be installed on the same server and shared among the same or different sets of clients.

SyncBox is based on the excellent `rsync`. It is supported on UNIX-like systems equipped with decently recent versions of:

* `rsync` 
* GNU `make` 
* `ssh` client (for the clients) or `ssh` server (for the SyncBox server)

It has been tested on Fedora, Debian, Ubuntu and Mac OS X operating systems. It should work unmodified or with minor changes on any UNIX-like system. There are no plans to support or test it on Microsoft Operating Systems. Please feel free to port it if you really cannot live without Microsoft environments but do not expect any help on this.

Using SyncBox is extremely simple once its basic principles are understood. The reference BOX on the server is not a working copy; it shall never be modified manually. Only a client's BOX can be a working copy and can be modified. Before using a BOX on a client, it must be opened by typing `make open` in its root directory. This synchronizes the server's BOX with the client's BOX so that the latter becomes a perfect copy of the former. It also creates a `.lock` file in the root directory of the two copies so that other clients are informed that the BOX is open and shall not be re-opened until it is closed by the same client that opened it. When the work with the BOX is finished on the client, the BOX must be closed by typing `make close` in its root directory. This synchronizes the client's BOX with the server's BOX so that the latter becomes a perfect copy of the former and removes the `.lock` files so that other clients can use their own BOX.

As a safety measure, if files are deleted or modified by the synchronizations, they are first saved in a subdirectory of the root directory of the BOX (on the client for `make open` synchronizations and on the server for `make close` synchronizations) named `.backup.TTT` where `TTT` is a unique timestamp (seconds since Epoch). `.lock` files and `.backup.TTT` directories are always ignored by the synchronizations. `.backup.TTT` directories must be deleted manually if needed. `.lock` files shall never be handled manually: the tool takes care of them automatically and relies on them to avoid conflicts between clients.

Important: SyncBox is not a version control system like `git`, `Mercurial` or `Subversion`. You can use such systems instead of SyncBox, they will provide the same basic functionality, plus the history of all modifications, plus many other very interesting features. Please have look at their documentation to learn more about version control systems. So, why using SyncBox instead of, let us say, Mercurial?

* SyncBox is much simpler. For the basic functionality described above, version control systems would really be overkill.
* Version control systems are very good at managing text files like programs source code. They are far less useful for binary files like pictures or music.
* If SyncBox is not exactly what you want but not far from it, it will be very easy to adapt. Modifying `Mercurial` is also possible (it is free software) but probably more difficult and error prone.
* In many cases you do not care about the history of modifications, all you want is accessibility, safety and security. Accessibility is the main purpose of SyncBox: it give you access to your data, any time, anywhere. Safety is due to the multiple copies: as long as at least one remains, you cannot loose your precious data and you can always restore the destroyed copies. Security is due to the use of the `ssh` protocol for all exchanges: everything is enciphered before transfer and deciphered on the receiver's side. No eavesdropping during the transfers (but of course, it is your responsibility to protect your data in the different copies of the BOX: even if they cannot be eavesdropped during transfers they can still be retrieved by intruders on the server or on a client).

# <a name="Manual"></a>Manual

## <a name="New"></a>Creating a new BOX

A new BOX can be created on the server or on a client, either starting from an existing directory or from an empty one. Simply download the `Makefile` and copy it in the directory you want to turn into a BOX:

```
$ cd mydir
$ wget https://github.com/pacalet/syncbox/blob/master/Makefile
```

Then, edit the `Makefile` and change the definition of the 3 configuration variables:

* `RHOST`:       the fully qualified name of the remote BOX server (e.g. `host.domain.org`)
* `RUSER`:       the username of the owner of the BOX on `RHOST` (e.g. `mary`)
* `RREPOSITORY`: the path of the BOX, relative to `RUSER`'s home directory on `RHOST` (e.g. `Documents/mydir`)

If you created the BOX on the server, you are done. If you created the BOX on a client, type `make init` and voila!, your local BOX has been copied on the server and is ready to use by any of your clients.

## <a name="Fresh"></a>Getting a fresh copy of an existing BOX on a new client

Create an empty directory and download the `Makefile`:

```
$ mkdir mydir
$ wget https://github.com/pacalet/syncbox/blob/master/Makefile
```

Then, edit the `Makefile` and change the definition of the 3 configuration variables just like if you were creating a new BOX (see above). Type `make sync`. You are done. Alternately, to avoid the manual editing, you can also get a copy of the server's `Makefile` with `scp`:

```
$ scp mary@host.domain.org:Documents/mydir/Makefile .
$ make sync
```

## <a name="Using"></a>Using a BOX on a client

Descend in the BOX root directory and type `make open`. The BOX is now open, you can read its content and modify it. Once you are done with the BOX, close it with `make close`. Do not forget to close the BOX because it prevents other clients from accessing it.

## <a name="Deleting"></a>Deleting a copy of the BOX on a client

Descend in the BOX root directory and type `make close`, just in case you opened the BOX and forgot to close it. Then delete the whole BOX directory.

## <a name="Nuke"></a>Completely deleting a BOX

Delete all copies on the clients and on the server.

## <a name="Dry"></a>Dry runs

If you set an environment variable named `DRYRUN` to a non-empty string before opening or closing a BOX:

```
$ DRYRUN=1 make init/sync/open/close
```

the synchronization will be run with the `--dry-run` option. Files will not be actually transferred but `rsync` will print messages indicating what would have been transferred. The lock files are handled normally.

## <a name="Errors"></a>Error messages

If you do not follow the SyncBox usage protocol you will get one of the following error messages:

```
***** The remote repository already exists.
***** Aborting...
```
You tried to create a new BOX from a client (`make init`) but the BOX already exists on the server.

```
***** Found a lock file in the local copy:
mary@host1.domain.org
***** Aborting...
```
You tried to open a BOX on a client (`host1.domain.org`) but it is already open, by `mary` (that is, very likely yourself).

```
***** Found a lock file in the remote repository:
john@host2.domain.org
***** Aborting...
```
You tried to open a BOX on a client but it is already opened by `john` on the `host2.domain.org` client.

```
***** Could not create lock file in the remote repository.
***** Aborting...
```
You tried to open a BOX on a client but, for any reason, SyncBox could not create the `.lock` file on the server. Could it be that there is an access rights problem on the server? Is the user (`RUSER` in the `Makefile`) allowed to access the BOX (`RREPOSITORY` in the `Makefile`) on the server (`RHOST` in the `Makefile`)? Has she write access on the server's BOX?

```
***** Could not create local lock file.
***** Aborting...
```
You tried to open a BOX on a client but, for any reason, SyncBox could not create the local `.lock` file. Could it be that there is an access rights problem on your client? Do you have write access on the local BOX?

```
***** No lock file in the local copy.
***** Aborting...
```
You tried to close a BOX on a client but the BOX is not opened. If it is because you forgot to open the BOX before modifying its content, you should open the BOX. This will restore the BOX content, create a `.backup.TTT` sub-directory and save your modifications in it. Recover your modifications from the `.backup.TTT` subdirectory and close the BOX. If you are prevented from opening the BOX because another client holds the BOX, wait until the other client closes the BOX and proceed as suggested but be careful that your modifications do not conflict with modifications from the other client (remember that SyncBox is not a version control system?)

```
***** No lock file in the remote repository.
***** Aborting...
```
You tried to close a BOX on a client but there is no `.lock` file on the server. Something went wrong somewhere. This should never happen but if it does, it is probably that you manually deleted local or remote `.lock` files. It is time to look at the server's and all the clients' copies, compare the copies, synchronize what needs to be and purge the orphan `.lock` files.

```
***** Could not delete lock file in the remote repository.
***** Aborting...
```
You tried to close a BOX on a client but, for any reason, SyncBox could not delete the `.lock` file on the server. Could it be that there is an access rights problem on the server? Is the user (`RUSER` in the `Makefile`) allowed to access the BOX (`RREPOSITORY` in the `Makefile`) on the server (`RHOST` in the `Makefile`)? Has she write access on the server's BOX and on the `.lock` file it contains?

```
***** Could not delete local lock file.
***** Aborting...
```
You tried to close a BOX on a client but, for any reason, SyncBox could not delete the local `.lock` file. Could it be that there is an access rights problem on your client? Do you have write access on the local BOX and on the `.lock` file it contains?
